﻿using System;
using CrazyRam.Plugins.ArrayPool;

namespace CrazyRam.Core.Serialization.Json
{
    public static class CrazyJson
    {
        private static IJsonProviderBank _provider;

        /// <summary>
        /// Serialize T to utf-8 buffer and returns count
        /// </summary>
        public static int Serialize<T>(T data, ref byte[] buffer, int offset = 0)
        {
            var writer = new JsonWriter(buffer, offset);

            var builder = _provider.FetchBuilder<T>();
            builder.Serialize(ref writer, data, _provider);

            buffer = writer.Buffer;

            return writer.Offset - offset;
        }

        public static ArraySegment<byte> Serialize<T>(T data)
        {
            const int defaultCapacity = 512;

            var buffer = new byte[defaultCapacity];
            var writer = new JsonWriter(buffer);

            var builder = _provider.FetchBuilder<T>();
            builder.Serialize(ref writer, data, _provider);

            return new ArraySegment<byte>(buffer, 0, writer.Offset);
        }

        public static string SerializeToString<T>(T data)
        {
            const int defaultCapacity = 512;
            var buffer = CrazyArrayPool<byte>.Default.Rent(defaultCapacity);

            var length = Serialize(data, ref buffer);
            var result = StringEncoding.Utf8.GetString(buffer, 0, length);

            CrazyArrayPool<byte>.Default.Return(buffer);
            return result;
        }

        public static T Deserialize<T>(byte[] buffer, int offset = 0)
        {
            var reader = new JsonReader(buffer, offset);
            var builder = _provider.FetchBuilder<T>();

            return builder.Deserialize(ref reader, _provider);
        }

        public static T Deserialize<T>(string data)
        {
            var count = StringEncoding.Utf8.GetByteCount(data);
            var buffer = CrazyArrayPool<byte>.Default.Rent(count);
            StringEncoding.Utf8.GetBytes(data, 0, data.Length, buffer, 0);

            var result = Deserialize<T>(buffer);
            CrazyArrayPool<byte>.Default.Return(buffer);

            return result;
        }

        public static unsafe T Deserialize<T>(byte* data)
        {
            return default;
        }

#region Region

        public static void RegisterProvider(IJsonProviderBank provider)
        {
            _provider = provider;
        }

        public static void AppendProvider(IJsonBuilderProvider provider)
        {
            _provider?.Append(provider);
        }

        public static void ClearProviders()
        {
            _provider?.Clear();
        }

#endregion
    }
}
