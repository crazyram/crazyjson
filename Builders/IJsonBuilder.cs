﻿namespace CrazyRam.Core.Serialization.Json
{
    public interface IJsonBuilder
    {
    }

    public interface IJsonBuilder<T> : IJsonBuilder
    {
        void Serialize(ref JsonWriter writer, T value, IJsonBuilderProvider provider);

        T Deserialize(ref JsonReader reader, IJsonBuilderProvider provider);
    }
}
