namespace CrazyRam.Core.Serialization.Json
{
    public static class PrimitiveBuilders
    {
        public class ByteBuilder : IJsonBuilder<byte>
        {
            public static readonly ByteBuilder Default = new ByteBuilder();

            public void Serialize(ref JsonWriter writer, byte value, IJsonBuilderProvider provider)
            {
                writer.WriteByte(value);
            }

            public byte Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadByte();
            }
        }

        public class SByteBuilder : IJsonBuilder<sbyte>
        {
            public static readonly SByteBuilder Default = new SByteBuilder();

            public void Serialize(ref JsonWriter writer, sbyte value, IJsonBuilderProvider provider)
            {
                writer.WriteSbyte(value);
            }

            public sbyte Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadSbyte();
            }
        }

        public class Int16Builder : IJsonBuilder<short>
        {
            public static readonly Int16Builder Default = new Int16Builder();

            public void Serialize(ref JsonWriter writer, short value, IJsonBuilderProvider provider)
            {
                writer.WriteShort(value);
            }

            public short Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadShort();
            }
        }

        public class UInt16Builder : IJsonBuilder<ushort>
        {
            public static readonly UInt16Builder Default = new UInt16Builder();

            public void Serialize(ref JsonWriter writer, ushort value, IJsonBuilderProvider provider)
            {
                writer.WriteUshort(value);
            }

            public ushort Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadUshort();
            }
        }

        public class Int32Builder : IJsonBuilder<int>
        {
            public static readonly Int32Builder Default = new Int32Builder();

            public void Serialize(ref JsonWriter writer, int value, IJsonBuilderProvider provider)
            {
                writer.WriteInt(value);
            }

            public int Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadInt();
            }
        }

        public class UInt32Builder : IJsonBuilder<uint>
        {
            public static readonly UInt32Builder Default = new UInt32Builder();

            public void Serialize(ref JsonWriter writer, uint value, IJsonBuilderProvider provider)
            {
                writer.WriteUint(value);
            }

            public uint Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadUint();
            }
        }

        public class Int64Builder : IJsonBuilder<long>
        {
            public static readonly Int64Builder Default = new Int64Builder();

            public void Serialize(ref JsonWriter writer, long value, IJsonBuilderProvider provider)
            {
                writer.WriteLong(value);
            }

            public long Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadLong();
            }
        }

        public class UInt64Builder : IJsonBuilder<ulong>
        {
            public static readonly UInt64Builder Default = new UInt64Builder();

            public void Serialize(ref JsonWriter writer, ulong value, IJsonBuilderProvider provider)
            {
                writer.WriteUlong(value);
            }

            public ulong Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadUlong();
            }
        }

        public class SingleBuilder : IJsonBuilder<float>
        {
            public static readonly SingleBuilder Default = new SingleBuilder();

            public void Serialize(ref JsonWriter writer, float value, IJsonBuilderProvider provider)
            {
                writer.WriteSingle(value);
            }

            public float Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadSingle();
            }
        }

        public class DoubleBuilder : IJsonBuilder<double>
        {
            public static readonly DoubleBuilder Default = new DoubleBuilder();

            public void Serialize(ref JsonWriter writer, double value, IJsonBuilderProvider provider)
            {
                writer.WriteDouble(value);
            }

            public double Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadDouble();
            }
        }

        public class StringBuilder : IJsonBuilder<string>
        {
            public static readonly StringBuilder Default = new StringBuilder();

            public void Serialize(ref JsonWriter writer, string value, IJsonBuilderProvider provider)
            {
                writer.WriteString(value);
            }

            public string Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                return reader.ReadString();
            }
        }
    }
}