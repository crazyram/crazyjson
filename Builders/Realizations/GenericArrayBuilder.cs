﻿using System;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Serialization.Json
{
    public static class GenericArrayBuilder
    {
        public static void Serialize<T>(ref JsonWriter writer, T[] data, IJsonBuilderProvider provider)
        {
            if (data == null)
            {
                writer.WriteNull();
                return;
            }

            writer.WriteBeginArray();

            if (data.Length == 0)
            {
                writer.WriteEndArray();
                return;
            }

            var builder = provider.FetchBuilder<T>();

            builder.Serialize(ref writer, data[0], provider);

            for (int i = 1; i < data.Length; i++)
            {
                writer.WriteValueSeparator();
                builder.Serialize(ref writer, data[i], provider);
            }

            writer.WriteEndArray();
        }

        public static void SerializeWithCast<T, TV>(ref JsonWriter writer, TV[] data, IJsonBuilderProvider provider)
        {
            if (data == null)
            {
                writer.WriteNull();
                return;
            }

            writer.WriteBeginArray();

            if (data.Length == 0)
            {
                writer.WriteEndArray();
                return;
            }

            var builder = provider.FetchBuilder<T>();

            builder.Serialize(ref writer, Unsafe.As<TV, T>(ref data[0]), provider);

            for (int i = 1; i < data.Length; i++)
            {
                writer.WriteValueSeparator();
                builder.Serialize(ref writer, Unsafe.As<TV, T>(ref data[i]), provider);
            }

            writer.WriteEndArray();
        }

        public static T[] Deserialize<T>(ref JsonReader reader, IJsonBuilderProvider provider, int defaultCapacity = 8)
        {
            if (reader.ReadIsNull())
                return null;
            var builder = provider.FetchBuilder<T>();

            reader.ReadIsBeginArray();
            var data = new T[defaultCapacity];
            int count = 0;
            while (!reader.ReadIsEndArrayWithValueSeparator(ref count))
            {
                if (data.Length < count)
                    Array.Resize(ref data, count * 2);
                data[count - 1] = builder.Deserialize(ref reader, provider);
            }

            Array.Resize(ref data, count);
            return data;
        }

        public static TV[] DeserializeWithCast<T, TV>(ref JsonReader reader, IJsonBuilderProvider provider,
            int defaultCapacity = 8)
        {
            if (reader.ReadIsNull())
                return null;
            var builder = provider.FetchBuilder<T>();

            reader.ReadIsBeginArray();
            var data = new TV[defaultCapacity];
            int count = 0;
            while (!reader.ReadIsEndArrayWithValueSeparator(ref count))
            {
                if (data.Length < count)
                    Array.Resize(ref data, count * 2);
                var result = builder.Deserialize(ref reader, provider);
                data[count - 1] = Unsafe.As<T, TV>(ref result);
            }

            Array.Resize(ref data, count);
            return data;
        }
    }
}
