﻿using Unity.Mathematics;

namespace CrazyRam.Core.Serialization.Json
{
    public static class MathBuilders
    {
        public class Bool2Builder : IJsonBuilder<bool2>
        {
            public static readonly Bool2Builder Default = new Bool2Builder();

            private readonly byte[][] _stringByteKeys;

            public Bool2Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                };
            }

            public void Serialize(ref JsonWriter writer, bool2 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteBoolean(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteBoolean(value.y);

                writer.WriteEndObject();
            }

            public bool2 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadBoolean();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadBoolean();

                reader.ReadIsEndObject();
                return new bool2(x, y);
            }
        }

        public class Bool3Builder : IJsonBuilder<bool3>
        {
            public static readonly Bool3Builder Default = new Bool3Builder();

            private readonly byte[][] _stringByteKeys;

            public Bool3Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("z"),
                };
            }

            public void Serialize(ref JsonWriter writer, bool3 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteBoolean(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteBoolean(value.y);

                writer.WriteRaw(_stringByteKeys[2]);
                writer.WriteBoolean(value.z);

                writer.WriteEndObject();
            }

            public bool3 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadBoolean();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadBoolean();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var z = reader.ReadBoolean();

                reader.ReadIsEndObject();
                return new bool3(x, y, z);
            }
        }

        public class Bool4Builder : IJsonBuilder<bool4>
        {
            public static readonly Bool4Builder Default = new Bool4Builder();

            private readonly byte[][] _stringByteKeys;

            public Bool4Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("z"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("w"),
                };
            }

            public void Serialize(ref JsonWriter writer, bool4 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteBoolean(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteBoolean(value.y);

                writer.WriteRaw(_stringByteKeys[2]);
                writer.WriteBoolean(value.z);

                writer.WriteRaw(_stringByteKeys[3]);
                writer.WriteBoolean(value.w);

                writer.WriteEndObject();
            }

            public bool4 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadBoolean();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadBoolean();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var z = reader.ReadBoolean();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var w = reader.ReadBoolean();

                reader.ReadIsEndObject();
                return new bool4(x, y, z, w);
            }
        }

        public class Int2Builder : IJsonBuilder<int2>
        {
            public static readonly Int2Builder Default = new Int2Builder();

            private readonly byte[][] _stringByteKeys;

            public Int2Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                };
            }

            public void Serialize(ref JsonWriter writer, int2 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteInt(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteInt(value.y);

                writer.WriteEndObject();
            }

            public int2 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadInt();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadInt();

                reader.ReadIsEndObject();
                return new int2(x, y);
            }
        }

        public class Int3Builder : IJsonBuilder<int3>
        {
            public static readonly Int3Builder Default = new Int3Builder();

            private readonly byte[][] _stringByteKeys;

            public Int3Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("z"),
                };
            }

            public void Serialize(ref JsonWriter writer, int3 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteInt(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteInt(value.y);

                writer.WriteRaw(_stringByteKeys[2]);
                writer.WriteInt(value.z);

                writer.WriteEndObject();
            }

            public int3 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadInt();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadInt();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var z = reader.ReadInt();

                reader.ReadIsEndObject();
                return new int3(x, y, z);
            }
        }

        public class Int4Builder : IJsonBuilder<int4>
        {
            public static readonly Int4Builder Default = new Int4Builder();

            private readonly byte[][] _stringByteKeys;

            public Int4Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("z"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("w"),
                };
            }

            public void Serialize(ref JsonWriter writer, int4 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteInt(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteInt(value.y);

                writer.WriteRaw(_stringByteKeys[2]);
                writer.WriteInt(value.z);

                writer.WriteRaw(_stringByteKeys[3]);
                writer.WriteInt(value.w);

                writer.WriteEndObject();
            }

            public int4 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadInt();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadInt();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var z = reader.ReadInt();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var w = reader.ReadInt();

                reader.ReadIsEndObject();
                return new int4(x, y, z, w);
            }
        }

        public class Float2Builder : IJsonBuilder<float2>
        {
            public static readonly Float2Builder Default = new Float2Builder();

            private readonly byte[][] _stringByteKeys;

            public Float2Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                };
            }

            public void Serialize(ref JsonWriter writer, float2 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteSingle(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteSingle(value.y);

                writer.WriteEndObject();
            }

            public float2 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadSingle();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadSingle();

                reader.ReadIsEndObject();
                return new float2(x, y);
            }
        }

        public class Float3Builder : IJsonBuilder<float3>
        {
            public static readonly Float3Builder Default = new Float3Builder();

            private readonly byte[][] _stringByteKeys;

            public Float3Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("z"),
                };
            }

            public void Serialize(ref JsonWriter writer, float3 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteSingle(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteSingle(value.y);

                writer.WriteRaw(_stringByteKeys[2]);
                writer.WriteSingle(value.z);

                writer.WriteEndObject();
            }

            public float3 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadSingle();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadSingle();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var z = reader.ReadSingle();

                reader.ReadIsEndObject();
                return new float3(x, y, z);
            }
        }

        public class Float4Builder : IJsonBuilder<float4>
        {
            public static readonly Float4Builder Default = new Float4Builder();

            private readonly byte[][] _stringByteKeys;

            public Float4Builder()
            {
                _stringByteKeys = new[]
                {
                    JsonWriter.GetEncodedPropertyName("x"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("y"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("z"),
                    JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator("w"),
                };
            }

            public void Serialize(ref JsonWriter writer, float4 value, IJsonBuilderProvider provider)
            {
                writer.WriteBeginObject();

                writer.WriteRaw(_stringByteKeys[0]);
                writer.WriteSingle(value.x);

                writer.WriteRaw(_stringByteKeys[1]);
                writer.WriteSingle(value.y);

                writer.WriteRaw(_stringByteKeys[2]);
                writer.WriteSingle(value.z);

                writer.WriteRaw(_stringByteKeys[3]);
                writer.WriteSingle(value.w);

                writer.WriteEndObject();
            }

            public float4 Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {
                reader.ReadIsBeginObject();

                reader.ReadIsPropertyNameWithSeparator();
                var x = reader.ReadSingle();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var y = reader.ReadSingle();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var z = reader.ReadSingle();
                reader.ReadIsValueSeparator();

                reader.ReadIsPropertyNameWithSeparator();
                var w = reader.ReadSingle();

                reader.ReadIsEndObject();
                return new float4(x, y, z, w);
            }
        }
    }
}
