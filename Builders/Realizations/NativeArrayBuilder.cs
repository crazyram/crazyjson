﻿using System;
using System.Runtime.CompilerServices;
using CrazyRam.Core.Helpers;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace CrazyRam.Core.Serialization.Json
{
    public static class NativeArrayBuilder
    {
        public static void Serialize<T>(ref JsonWriter writer, NativeArray<T> data, IJsonBuilderProvider provider)
            where T : unmanaged
        {
            if (!data.IsCreated)
            {
                writer.WriteNull();
                return;
            }

            writer.WriteBeginArray();

            if (data.Length == 0)
            {
                writer.WriteEndArray();
                return;
            }

            var builder = provider.FetchBuilder<T>();

            builder.Serialize(ref writer, data[0], provider);

            for (int i = 1; i < data.Length; i++)
            {
                writer.WriteValueSeparator();
                builder.Serialize(ref writer, data[i], provider);
            }

            writer.WriteEndArray();
        }

        public static unsafe void SerializeWithCast<T, TV>(ref JsonWriter writer, NativeArray<TV> data,
            IJsonBuilderProvider provider)
            where TV : unmanaged
            where T : unmanaged
        {
            if (!data.IsCreated)
            {
                writer.WriteNull();
                return;
            }

            writer.WriteBeginArray();

            if (data.Length == 0)
            {
                writer.WriteEndArray();
                return;
            }

            var builder = provider.FetchBuilder<T>();

            var size = sizeof(TV);
            var offset = 0;
            var ptr = (IntPtr) data.GetUnsafePtr();

            // ref case:
            // (T&) ref (*(int*) source)
            builder.Serialize(ref writer, *(T*) ptr, provider);

            for (int i = 1; i < data.Length; i++)
            {
                offset += size;
                writer.WriteValueSeparator();
                builder.Serialize(ref writer, *(T*) (ptr + offset), provider);
            }

            writer.WriteEndArray();
        }

        public static NativeArray<T> Deserialize<T>(ref JsonReader reader, IJsonBuilderProvider provider,
            Allocator allocator, int defaultCapacity = 8) where T : unmanaged
        {
            if (reader.ReadIsNull())
                return default;
            const NativeArrayOptions clearOption = NativeArrayOptions.UninitializedMemory;
            var builder = provider.FetchBuilder<T>();

            var data = new NativeArray<T>(defaultCapacity, allocator, clearOption);

            reader.ReadIsBeginArray();
            int count = 0;
            while (!reader.ReadIsEndArrayWithValueSeparator(ref count))
            {
                if (data.Length < count)
                    data = data.Resize(count * 2, data.Length, allocator, clearOption);
                data[count - 1] = builder.Deserialize(ref reader, provider);
            }

            if (data.Length > count)
                data = data.Resize(count, count, allocator, clearOption);
            return data;
        }

        public static NativeArray<TV> DeserializeWithCast<T, TV>(ref JsonReader reader, IJsonBuilderProvider provider,
            Allocator allocator, int defaultCapacity = 8)
            where T : unmanaged
            where TV : unmanaged
        {
            if (reader.ReadIsNull())
                return default;
            const NativeArrayOptions clearOption = NativeArrayOptions.UninitializedMemory;
            var builder = provider.FetchBuilder<T>();

            reader.ReadIsBeginArray();
            var data = new NativeArray<TV>(defaultCapacity, allocator, clearOption);
            int count = 0;
            while (!reader.ReadIsEndArrayWithValueSeparator(ref count))
            {
                if (data.Length < count)
                    data = data.Resize(count * 2, data.Length, allocator, clearOption);
                var result = builder.Deserialize(ref reader, provider);
                data[count - 1] = Unsafe.As<T, TV>(ref result);
            }

            if (data.Length > count)
                data = data.Resize(count, count, allocator, clearOption);
            return data;
        }
    }
}
