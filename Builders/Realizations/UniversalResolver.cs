﻿using System;
using System.Linq;

namespace CrazyRam.Core.Serialization.Json
{
    public sealed class UniversalResolver : IJsonProviderBank
    {
        private static IJsonBuilderProvider[] _providers =
        {
            new PrimitiveProvider(),
            new BaseMathProvider(),
        };

#region Setup

        public void Append(IJsonBuilderProvider provider)
        {
            _providers = _providers.Append(provider).ToArray();
        }

        public void Clear()
        {
            _providers = Array.Empty<IJsonBuilderProvider>();
        }

        public void Register(IJsonBuilderProvider[] providers)
        {
            _providers = providers;
        }

#endregion

        public IJsonBuilder<T> FetchBuilder<T>()
        {
            return BuilderCache<T>.Cache;
        }

        private static class BuilderCache<T>
        {
            public static readonly IJsonBuilder<T> Cache;

            static BuilderCache()
            {
                for (int i = 0; i < _providers.Length; i++)
                {
                    var builder = _providers[i].FetchBuilder<T>();
                    if (builder == null)
                        continue;
                    Cache = builder;
                    return;
                }

                throw new JsonBuilderNotRegisteredException($"{typeof(T).Name} builder is not registered");
            }
        }
    }
}
