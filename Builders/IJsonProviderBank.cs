﻿namespace CrazyRam.Core.Serialization.Json
{
    public interface IJsonProviderBank : IJsonBuilderProvider
    {
        void Register(IJsonBuilderProvider[] providers);

        void Append(IJsonBuilderProvider provider);

        void Clear();
    }
}
