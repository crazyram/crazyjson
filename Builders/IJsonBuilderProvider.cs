﻿namespace CrazyRam.Core.Serialization.Json
{
    public interface IJsonBuilderProvider
    {
        IJsonBuilder<T> FetchBuilder<T>();
    }
}
