﻿using System;
using System.Collections.Generic;
using Unity.Mathematics;

namespace CrazyRam.Core.Serialization.Json
{
    public class BaseMathProvider : IJsonBuilderProvider
    {
        private static readonly Dictionary<Type, IJsonBuilder> Builders = new Dictionary<Type, IJsonBuilder>
        {
            {typeof(int2), MathBuilders.Int2Builder.Default},
            {typeof(int3), MathBuilders.Int3Builder.Default},
            {typeof(int4), MathBuilders.Int4Builder.Default},

            {typeof(float2), MathBuilders.Float2Builder.Default},
            {typeof(float3), MathBuilders.Float3Builder.Default},
            {typeof(float4), MathBuilders.Float4Builder.Default},

            {typeof(bool2), MathBuilders.Bool2Builder.Default},
            {typeof(bool3), MathBuilders.Bool3Builder.Default},
            {typeof(bool4), MathBuilders.Bool4Builder.Default},
        };

        public IJsonBuilder<T> FetchBuilder<T>()
        {
            return Builders.TryGetValue(typeof(T), out var match) && match is IJsonBuilder<T> builder
                ? builder
                : null;
        }
    }
}
