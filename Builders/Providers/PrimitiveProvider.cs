﻿using System;
using System.Collections.Generic;

namespace CrazyRam.Core.Serialization.Json
{
    public class PrimitiveProvider : IJsonBuilderProvider
    {
        private static readonly Dictionary<Type, IJsonBuilder> Builders = new Dictionary<Type, IJsonBuilder>
        {
            {typeof(byte), PrimitiveBuilders.ByteBuilder.Default},
            {typeof(sbyte), PrimitiveBuilders.SByteBuilder.Default},
            {typeof(short), PrimitiveBuilders.Int16Builder.Default},
            {typeof(ushort), PrimitiveBuilders.UInt16Builder.Default},
            {typeof(int), PrimitiveBuilders.Int32Builder.Default},
            {typeof(uint), PrimitiveBuilders.UInt32Builder.Default},
            {typeof(long), PrimitiveBuilders.Int64Builder.Default},
            {typeof(ulong), PrimitiveBuilders.UInt64Builder.Default},
            {typeof(float), PrimitiveBuilders.SingleBuilder.Default},
            {typeof(double), PrimitiveBuilders.DoubleBuilder.Default},
            {typeof(string), PrimitiveBuilders.StringBuilder.Default},
        };

        public IJsonBuilder<T> FetchBuilder<T>()
        {
            return Builders.TryGetValue(typeof(T), out var match) && match is IJsonBuilder<T> builder
                ? builder
                : null;
        }
    }
}
