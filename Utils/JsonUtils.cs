﻿using System;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Serialization.Json
{
    public static class JsonUtils
    {
        private const int ArrayMaxSize = 0x7FFFFFC7;

        // [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void EnsureCapacity(ref byte[] bytes, int offset, int appendLength)
        {
            var newLength = offset + appendLength;

            if (bytes == null)
            {
                bytes = new byte[newLength];
                return;
            }


            var current = bytes.Length;
            if (newLength > current)
            {
                int num = newLength;
                if (num < 256)
                {
                    num = 256;
                    FastResize(ref bytes, num);
                    return;
                }

                if (current == ArrayMaxSize)
                {
                    throw new InvalidOperationException("byte[] size overflow");
                }

                var newSize = unchecked((current * 2));
                if (newSize < 0) // overflow
                {
                    num = ArrayMaxSize;
                }
                else
                {
                    if (num < newSize)
                    {
                        num = newSize;
                    }
                }

                FastResize(ref bytes, num);
            }
        }

        // Buffer.BlockCopy version of Array.Resize
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void FastResize(ref byte[] array, int newSize)
        {
            if (newSize < 0)
                throw new ArgumentOutOfRangeException(nameof(newSize));

            var ptrCopy = array;
            if (ptrCopy == null)
            {
                array = new byte[newSize];
                return;
            }

            if (ptrCopy.Length == newSize)
                return;
            var result = new byte[newSize];
            Buffer.BlockCopy(ptrCopy, 0, result, 0, (ptrCopy.Length > newSize) ? newSize : ptrCopy.Length);
            array = result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe byte[] FastCloneWithResize(byte[] src, int newSize)
        {
            if (newSize < 0)
                throw new ArgumentOutOfRangeException(nameof(newSize));

            if (src == null)
                return new byte[newSize];

            if (src.Length < newSize)
                throw new ArgumentException("length < newSize");

            var dst = new byte[newSize];

#if UNITY_2019_3
            fixed (byte* pSrc = &src[0])
            fixed (byte* pDst = &dst[0])
            {
                Buffer.MemoryCopy(pSrc, pDst, dst.Length, newSize);
            }
#else
            Buffer.BlockCopy(src, 0, dst, 0, newSize);
#endif

            return dst;
        }
    }
}
