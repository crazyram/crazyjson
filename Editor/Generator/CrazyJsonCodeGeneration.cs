﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CrazyRam.Core.Serialization.Json;
using Unity.Collections;
using UnityEditor;

namespace CrazyRam.Editor.Json
{
    public static class CrazyJsonCodeGeneration
    {
        private const string PrimitiveTemplate = @"namespace CrazyRam.Core.Serialization.Json
{{
    public static class PrimitiveBuilders
    {{{0}    }}
}}";

        /// <summary>
        /// 0 - type name
        /// 1 - short type name
        /// 2 - write method
        /// 3 - read method
        /// </summary>
        private const string PrimitiveBuilderTemplate = @"
        public class {0}JsonBuilder : IJsonBuilder<{1}>
        {{
            public static readonly {0}Builder Default = new {0}Builder();

            public void Serialize(ref JsonWriter writer, {1} value, IJsonBuilderProvider provider)
            {{
                writer.{2}(value);
            }}

            public {1} Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
            {{
                return reader.{3}();
            }}
        }}
";

        /// <summary>
        /// 0 - using block
        /// 1 - source builders
        /// </summary>
        private const string CustomTemplate = @"{0}namespace CrazyRam.Core.Serialization.Json
{{{1}}}
";

        /// <summary>
        /// 0 - type name
        /// 1 - member names
        /// </summary>
        private const string BinderInitialization = @"        private readonly byte[][] _stringByteKeys;

        public {0}JsonBuilder()
        {{
            _stringByteKeys = new[]
            {{{1}
            }};
        }}
";

        /// <summary>
        /// 0 - type name
        /// 1 - initialization
        /// 2 - null check
        /// 3 - fields write
        /// 4 - fields read
        /// 5 - constructor arguments
        /// </summary>
        private const string CustomBuilderTemplate = @"
    public class {0}JsonBuilder : IJsonBuilder<{0}>
    {{
{1}
        public void Serialize(ref JsonWriter writer, {0} value, IJsonBuilderProvider provider)
        {{{2}
            writer.WriteBeginObject();{3}

            writer.WriteEndObject();
        }}

        public {0} Deserialize(ref JsonReader reader, IJsonBuilderProvider provider)
        {{
            reader.ReadIsBeginObject();{4}

            reader.ReadIsEndObject();
            return new {0}({5});
        }}
    }}
";

        private const string NullCheckPattern = @"
        if (value == null)
        {
            writer.WriteNull();
            return;
        }
";

        private const string WritePattern = "Write{0}";

        private const string ReadPattern = "Read{0}";

        /// <summary>
        /// 0 - member id
        /// 1 - write method
        /// </summary>
        private const string WriteMember = @"

            writer.WriteRaw(_stringByteKeys[{0}]);
{1}";

        /// <summary>
        /// 0 - read method
        /// 1 - read separator
        /// </summary>
        private const string ReadMember = @"

            reader.ReadIsPropertyNameWithSeparator();
{0}{1}";

        /// <summary>
        /// 0 - read type
        /// 1 - read property name
        /// 2 - json property name
        /// 3 - read method
        /// 4 - read separator
        /// </summary>
        private const string ReadOptionalMember = @"

            {0} {1} = default;
            if (reader.IsPropertyNameMatch(""\""{2}\""""))
            {{
                reader.ReadIsPropertyNameWithSeparator();
{3}{4}
            }}";

        /// <summary>
        /// 0 - write name
        /// 1 - cast type of empty
        /// 2 - member name
        /// </summary>
        private const string PrimitiveWritePattern = @"            writer.Write{0}({1}value.{2});";

        /// <summary>
        /// 0 - variable name
        /// 1 - cast type or empty
        /// 2 - write name
        /// </summary>
        private const string PrimitiveReadPattern = @"            var {0} = {1}reader.Read{2}();";

        /// <summary>
        /// 0 - variable name
        /// 1 - cast type or empty
        /// 2 - write name
        /// </summary>
        private const string NamedPrimitiveReadPattern = @"                {0} = {1}reader.Read{2}();";

        private const string FirstNamePattern = @"
                JsonWriter.GetEncodedPropertyName(""{0}""),";

        private const string NamePattern = @"
                JsonWriter.GetEncodedPropertyNameWithPrefixValueSeparator(""{0}""),";

        /// <summary>
        /// 0 - type
        /// 1 - member name
        /// </summary>
        private const string GenericWritePattern =
            @"            provider.FetchBuilder<{0}>().Serialize(ref writer, value.{1}, provider);
";

        /// <summary>
        /// 0 - variable name
        /// 1 - type
        /// </summary>
        private const string GenericReadPattern =
            @"            var {0} = provider.FetchBuilder<{1}>().Deserialize(ref reader, provider);
";

        /// <summary>
        /// 0 - variable name
        /// 1 - type
        /// </summary>
        private const string NamedGenericReadPattern =
            @"                {0} = provider.FetchBuilder<{1}>().Deserialize(ref reader, provider);
";

#region Array

        /// <summary>
        /// 0 - array member name
        /// </summary>
        private const string ArrayWriteTemplate =
            @"            GenericArrayBuilder.Serialize(ref writer, value.{0}, provider);";

        /// <summary>
        /// 0 - variable name
        /// 1 - type name
        /// 2 - capacity
        /// </summary>
        private const string ArrayReadTemplate =
            @"            var {0} = GenericArrayBuilder.Deserialize<{1}>(ref reader, provider{2});";

        /// <summary>
        /// 0 - variable name
        /// 1 - type name
        /// 2 - capacity
        /// </summary>
        private const string NamedArrayReadTemplate =
            @"                {0} = GenericArrayBuilder.Deserialize<{1}>(ref reader, provider{2});";

        /// <summary>
        /// 0 - real serialize type
        /// 1 - generic type(e.g. enum)
        /// 2 - target array
        /// </summary>
        private const string ArrayCastWriteTemplate =
            @"            GenericArrayBuilder.SerializeWithCast<{0}, {1}>(ref writer, value.{2}, provider);";

        /// <summary>
        /// 0 - variable name
        /// 1 - real serialize type
        /// 2 - result type
        /// 3 - capacity
        /// </summary>
        private const string ArrayCastReadTemplate =
            @"            var {0} = GenericArrayBuilder.DeserializeWithCast<{1}, {2}>(ref reader, provider{2});";

        /// <summary>
        /// 0 - variable name
        /// 1 - real serialize type
        /// 2 - result type
        /// 3 - capacity
        /// </summary>
        private const string NamedArrayCastReadTemplate =
            @"                {0} = GenericArrayBuilder.DeserializeWithCast<{1}, {2}>(ref reader, provider{2});";

#endregion

#region NativeArray

        /// <summary>
        /// 0 - array member name
        /// </summary>
        private const string NativeArrayWriteTemplate =
            @"            NativeArrayBuilder.Serialize(ref writer, value.{0}, provider);";

        /// <summary>
        /// 0 - variable name
        /// 1 - type name
        /// 2 - allocator
        /// 3 - capacity(optional)
        /// </summary>
        private const string NativeArrayReadTemplate =
            @"            var {0} = NativeArrayBuilder.Deserialize<{1}>(ref reader, provider, Allocator.{2}{3});";

        /// <summary>
        /// 0 - variable name
        /// 1 - type name
        /// 2 - allocator
        /// 3 - capacity(optional)
        /// </summary>
        private const string NativeNamedArrayReadTemplate =
            @"                {0} = NativeArrayBuilder.Deserialize<{1}>(ref reader, provider, Allocator.{2}{3});";

        /// <summary>
        /// 0 - real serialize type
        /// 1 - generic type(e.g. enum)
        /// 2 - target array
        /// </summary>
        private const string NativeArrayCastWriteTemplate =
            @"            NativeArrayBuilder.SerializeWithCast<{0}, {1}>(ref writer, value.{2}, provider);";

        /// <summary>
        /// 0 - variable name
        /// 1 - real serialize type
        /// 2 - result type
        /// 3 - allocator
        /// 4 - capacity(optional)
        /// </summary>
        private const string NativeArrayCastReadTemplate =
            @"            var {0} = NativeArrayBuilder.DeserializeWithCast<{1}, {2}>(ref reader, provider, Allocator.{3}{4});";

        /// <summary>
        /// 0 - variable name
        /// 1 - real serialize type
        /// 2 - result type
        /// 3 - allocator
        /// 4 - capacity(optional)
        /// </summary>
        private const string NativeNamedArrayCastReadTemplate =
            @"                {0} = NativeArrayBuilder.DeserializeWithCast<{1}, {2}>(ref reader, provider, Allocator.{3}{4});";

#endregion

        private const string ProviderEntryTemplate = @"
            {{typeof({0}), new {0}JsonBuilder()}},";

        private const string UsingTemplate = @"using {0};
";

        private const string ReadValueSeparatorTemplate = @"
            reader.ReadIsValueSeparator();";

        private const string GeneratedProviderTemplate = @"using System;
using System.Collections.Generic;
{0}
namespace CrazyRam.Core.Serialization.Json
{{
    public class JsonGeneratedProvider : IJsonBuilderProvider
    {{
        private static readonly Dictionary<Type, IJsonBuilder> Builders = new Dictionary<Type, IJsonBuilder>
        {{{1}
        }};

        public IJsonBuilder<T> FetchBuilder<T>()
        {{
            return Builders.TryGetValue(typeof(T), out var match) && match is IJsonBuilder<T> builder
                ? builder
                : null;
        }}
    }}
}}
";

        private const string PrimitiveBuilderScriptName = "PrimitiveBuilders.cs";

        private const string CustomBuilderScriptName = "JsonGeneratedBindings.cs";

        private const string CustomProviderScriptName = "JsonGeneratedProvider.cs";

        private static readonly Dictionary<Type, string> PrimitiveTypeToNameMapping = new Dictionary<Type, string>
        {
            {typeof(byte), "Byte"},
            {typeof(sbyte), "Sbyte"},
            {typeof(short), "Short"},
            {typeof(ushort), "Ushort"},
            {typeof(int), "Int"},
            {typeof(uint), "Uint"},
            {typeof(long), "Long"},
            {typeof(ulong), "Ulong"},
            {typeof(float), "Single"},
            {typeof(double), "Double"},
            {typeof(string), "String"},
            {typeof(bool), "Boolean"},
        };

        private static readonly HashSet<string> ReservedNamesList = new HashSet<string>
        {
            "params", "event"
        };

        private static void PrimitiveBuildersWriter(TextWriter writer)
        {
            var sb = new StringBuilder();
            foreach (var pair in PrimitiveTypeToNameMapping)
            {
                var builder = string.Format(PrimitiveBuilderTemplate,
                    pair.Key.Name,
                    pair.Key.GetFriendlyTypeName(),
                    string.Format(WritePattern, pair.Value),
                    string.Format(ReadPattern, pair.Value)
                );
                sb.Append(builder);
            }

            writer.Write(PrimitiveTemplate, sb);
        }

        private static bool IsJsonSerializable(MemberInfo type)
        {
            return type.GetCustomAttribute<JsonSerializableAttribute>() != null;
        }

        private readonly struct JsonArrayField
        {
            public readonly string CountName;

            public readonly int ArrayCapacity;

            public readonly Allocator Allocator;

            public JsonArrayField(string countName, Allocator allocator, int arrayCapacity)
            {
                CountName = countName;
                Allocator = allocator;
                ArrayCapacity = arrayCapacity;
            }
        }

        private readonly struct JsonField
        {
            public readonly string JsonName;

            public readonly string MemberName;


            public readonly int Order;

            public readonly Type Type;

            public readonly JsonArrayField ArrayData;

            public readonly bool IsOptional;

            public JsonField(string jsonName, string memberName, Type type, int order, bool optional = false,
                JsonArrayField arrayData = default)
            {
                JsonName = jsonName;
                MemberName = memberName;
                Order = order;
                Type = type;
                IsOptional = optional;

                ArrayData = arrayData;
            }
        }

        private static JsonArrayField FetchArrayData(MemberInfo info)
        {
            var arrayCapacity = info.GetCustomAttribute<JsonArrayCapacityAttribute>();
            var allocator = info.GetCustomAttribute<JsonNativeAllocatorAttribute>();
            var countProperty = info.GetCustomAttribute<JsonArrayCountPropertyAttribute>();

            return new JsonArrayField(
                countProperty?.Property,
                allocator?.Allocator ?? Allocator.Temp,
                arrayCapacity?.Capacity ?? 0
            );
        }

        private static bool IsJsonMember(MemberInfo info)
        {
            return info.GetCustomAttribute<JsonDataAttribute>() != null;
        }

        private readonly struct JsonBuilderData
        {
            public readonly Type Type;

            public readonly JsonField[] Fields;

            public JsonBuilderData(Type type)
            {
                Type = type;

                var fields = type
                    .GetFields()
                    .Where(IsJsonMember)
                    .Select(info =>
                    {
                        var attribute = info.GetCustomAttribute<JsonDataAttribute>();
                        var isOptional = info.GetCustomAttribute<JsonOptionalAttribute>();
                        var arrayData = FetchArrayData(info);

                        return new JsonField(
                            !string.IsNullOrEmpty(attribute.NameOverride) ? attribute.NameOverride : info.Name,
                            info.Name, info.FieldType, attribute.Id, isOptional != null, arrayData);
                    });
                var properties = type
                    .GetProperties()
                    .Where(IsJsonMember)
                    .Select(info =>
                    {
                        var attribute = info.GetCustomAttribute<JsonDataAttribute>();
                        var isOptional = info.GetCustomAttribute<JsonOptionalAttribute>();
                        var arrayData = FetchArrayData(info);

                        return new JsonField(
                            !string.IsNullOrEmpty(attribute.NameOverride) ? attribute.NameOverride : info.Name,
                            info.Name, info.PropertyType, attribute.Id, isOptional != null, arrayData);
                    });

                Fields = fields
                    .Concat(properties)
                    .ToArray()
                    .OrderBy(field => field.Order)
                    .ToArray();
            }
        }

        private static string FixReservedKeywordsNaming(string value)
        {
            return ReservedNamesList.Contains(value) ? $"@{value}" : value;
        }

        private static string ToVariableName(string value)
        {
            if (string.IsNullOrEmpty(value) || char.IsLower(value[0]))
                return FixReservedKeywordsNaming(value);
            return value.Length == 1
                ? char.ToLower(value[0]).ToString()
                : FixReservedKeywordsNaming($"{char.ToLower(value[0])}{value.Substring(1)}");
        }

        private static string BuildCapacityString(in JsonArrayField data)
        {
            var hasCapacity = data.ArrayCapacity > 0;
            var hasCountName = !string.IsNullOrEmpty(data.CountName);
            if (!hasCapacity && !hasCountName)
                return string.Empty;
            return hasCountName ? $", {ToVariableName(data.CountName)}" : $", {data.ArrayCapacity}";
        }

        private static void CustomBuildersWriter(TextWriter writer, IEnumerable<JsonBuilderData> types)
        {
            var builders = new StringBuilder();

            var names = new StringBuilder();
            var fmtWriter = new StringBuilder();
            var fmtReader = new StringBuilder();
            var constructor = new StringBuilder();
            var namespaces = new HashSet<string>();

            foreach (var jsonType in types)
            {
                names.Length = 0;
                fmtWriter.Length = 0;
                fmtReader.Length = 0;
                constructor.Length = 0;

                namespaces.Add(jsonType.Type.GetFullyTypeNamespace());

                for (int i = 0; i < jsonType.Fields.Length; i++)
                {
                    ref readonly var field = ref jsonType.Fields[i];

                    names.Append(string.Format(i == 0 ? FirstNamePattern : NamePattern, field.JsonName));
                    // GetEncodedPropertyNameWithBeginObject

                    var memberType = field.Type;
                    var writeCast = string.Empty;
                    var readCast = string.Empty;
                    if (field.Type.IsEnum)
                    {
                        memberType = field.Type.GetEnumUnderlyingType();
                        writeCast = $"({memberType.GetFriendlyTypeName()})";
                        readCast = $"({field.Type.GetFullyShortTypeName()})";
                    }

                    var variableName = ToVariableName(field.MemberName);

                    string writeMethod;
                    string readMethod;

                    if (PrimitiveTypeToNameMapping.ContainsKey(memberType))
                    {
                        var method = PrimitiveTypeToNameMapping[memberType];

                        // 0 - write name
                        // 1 - cast type of empty
                        // 2 - member name
                        writeMethod = string.Format(PrimitiveWritePattern, method, writeCast, field.MemberName);

                        // 0 - variable name
                        // 1 - cast type or empty
                        // 2 - write name
                        readMethod = string.Format(
                            field.IsOptional ? NamedPrimitiveReadPattern : PrimitiveReadPattern,
                            variableName, readCast, method);
                    }
                    else if (memberType.IsArray)
                    {
                        memberType = memberType.GetElementType();
                        if (memberType == null)
                            throw new Exception($"Cannot serialize type: {field.Type.GetFullyShortTypeName()}");
                        if (memberType.IsEnum)
                        {
                            var underlyingType = memberType.GetEnumUnderlyingType().GetFriendlyTypeName();
                            // 0 - real serialize type
                            // 1 - generic type(e.g. enum)
                            // 2 - target array
                            writeMethod = string.Format(ArrayCastWriteTemplate, underlyingType, memberType,
                                field.MemberName);

                            // 0 - variable name
                            // 1 - real serialize type
                            // 2 - result type
                            // 3 - capacity
                            readMethod = string.Format(
                                field.IsOptional ? NamedArrayCastReadTemplate : ArrayCastReadTemplate,
                                variableName, underlyingType, memberType, BuildCapacityString(in field.ArrayData));
                        }
                        else
                        {
                            writeMethod = string.Format(ArrayWriteTemplate, field.MemberName);

                            // 0 - variable name
                            // 1 - type name
                            // 2 - capacity
                            readMethod = string.Format(
                                field.IsOptional ? NamedArrayReadTemplate : ArrayReadTemplate,
                                variableName, memberType.GetFriendlyTypeName(),
                                BuildCapacityString(in field.ArrayData));
                        }
                    }
                    else if (memberType.IsGenericType && memberType.GetGenericTypeDefinition() == typeof(NativeArray<>))
                    {
                        memberType = memberType.GenericTypeArguments[0];
                        if (memberType == null)
                            throw new Exception($"Cannot serialize type: {field.Type.GetFullyShortTypeName()}");
                        if (memberType.IsEnum)
                        {
                            var underlyingType = memberType.GetEnumUnderlyingType().GetFriendlyTypeName();
                            // 0 - real serialize type
                            // 1 - generic type(e.g. enum)
                            // 2 - target array
                            writeMethod = string.Format(NativeArrayCastWriteTemplate, underlyingType, memberType,
                                field.MemberName);


                            // 0 - variable name
                            // 1 - real serialize type
                            // 2 - result type
                            // 3 - allocator
                            // 4 - capacity(optional)
                            readMethod = string.Format(
                                field.IsOptional ? NativeNamedArrayCastReadTemplate : NativeArrayCastReadTemplate,
                                variableName, underlyingType, memberType, field.ArrayData.Allocator,
                                BuildCapacityString(in field.ArrayData));
                        }
                        else
                        {
                            writeMethod = string.Format(NativeArrayWriteTemplate, field.MemberName);

                            // 0 - variable name
                            // 1 - type name
                            // 2 - allocator
                            // 3 - capacity(optional)
                            readMethod = string.Format(
                                field.IsOptional ? NativeNamedArrayReadTemplate : NativeArrayReadTemplate,
                                variableName, memberType.GetFriendlyTypeName(), field.ArrayData.Allocator,
                                BuildCapacityString(in field.ArrayData));
                        }
                    }
                    else
                    {
                        // 0 - type
                        // 1 - member name
                        writeMethod = string.Format(GenericWritePattern, memberType.GetFullyShortTypeName(),
                            field.MemberName);

                        // 0 - variable name
                        // 1 - type
                        readMethod = string.Format(
                            field.IsOptional ? NamedGenericReadPattern : GenericReadPattern,
                            variableName, memberType.GetFullyShortTypeName());
                    }

                    bool isNotLast = i < jsonType.Fields.Length - 1;

                    // 0 - member id
                    // 1 - write method
                    fmtWriter.Append(string.Format(WriteMember, i, writeMethod));

                    if (field.IsOptional)
                    {
                        // 0 - read type
                        // 1 - read property name
                        // 2 - json property name
                        // 3 - read method
                        // 4 - read separator
                        fmtReader.Append(string.Format(ReadOptionalMember, memberType.GetFriendlyTypeName(),
                            variableName, field.JsonName, readMethod,
                            isNotLast ? ReadValueSeparatorTemplate : string.Empty));
                    }
                    else
                    {
                        // 0 - read method
                        // 1 - read separator
                        fmtReader.Append(string.Format(ReadMember, readMethod,
                            isNotLast ? ReadValueSeparatorTemplate : string.Empty));
                    }

                    constructor.Append(variableName);
                    if (isNotLast)
                        constructor.Append(", ");
                }

                // 0 - type name
                // 1 - member names
                var initialization = jsonType.Fields.Length == 0
                    ? string.Empty
                    : string.Format(BinderInitialization, jsonType.Type.Name, names);

                // 0 - type name
                // 1 - member names
                // 2 - null check
                // 3 - fields write
                // 4 - fields read
                // 5 - constructor arguments
                builders.Append(string.Format(CustomBuilderTemplate, jsonType.Type.Name,
                    initialization, jsonType.Type.IsValueType ? string.Empty : NullCheckPattern, fmtWriter, fmtReader,
                    constructor));
            }

            var usingBlock = string.Concat(namespaces.Select(space =>
                string.IsNullOrEmpty(space) || string.Equals(space, "Null")
                    ? string.Empty
                    : string.Format(UsingTemplate, space)));
            if (usingBlock.Length > 0)
                usingBlock += "\r\n";
            writer.Write(CustomTemplate, usingBlock, builders);
        }

        private static void CustomProviderWriter(TextWriter writer, IEnumerable<JsonBuilderData> types)
        {
            var providers = new StringBuilder();
            var namespaces = new HashSet<string>();
            foreach (var type in types)
            {
                namespaces.Add(type.Type.GetFullyTypeNamespace());
                providers.Append(string.Format(ProviderEntryTemplate, type.Type.Name));
            }

            var usingBlock = string.Concat(namespaces.Select(space =>
                string.IsNullOrEmpty(space) || string.Equals(space, "Null")
                    ? string.Empty
                    : string.Format(UsingTemplate, space)));
            writer.Write(GeneratedProviderTemplate, usingBlock, providers);
        }

        private static IEnumerable<JsonBuilderData> SelectTypes()
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(assembly => assembly
                    .GetTypes().Where(type => (type.IsClass || type.IsValueType) && IsJsonSerializable(type)))
                .Select(type => new JsonBuilderData(type));
        }

        // [MenuItem(ScriptableObjectPathHelper.EditorAsset + "GenerateJsonPrimitives")]
        public static void ExportPrimitiveBuilders()
        {
            CodeGenerationHelper.WriteScript(CodeGenerationHelper.GetFileName(PrimitiveBuilderScriptName),
                PrimitiveBuildersWriter);
        }

        [MenuItem("CrazyRam/Editor/Serialization/JSON/RebuildSchema")]
        public static void ExportJsonBuilders()
        {
            var types = SelectTypes().ToArray();
            CodeGenerationHelper.WriteScript(CodeGenerationHelper.GetFileName(CustomBuilderScriptName), types,
                CustomBuildersWriter);
            CodeGenerationHelper.WriteScript(CodeGenerationHelper.GetFileName(CustomProviderScriptName), types,
                CustomProviderWriter);
        }
    }
}
