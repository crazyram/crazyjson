﻿using CrazyRam.Core.Serialization.Json;
using NUnit.Framework;
using UnityEngine;

namespace CrazyRam.Editor
{
    public class JsonAdapterTests
    {
        private struct SomeTestHeader
        {
            public readonly int Id;

            public readonly string Name;

            public readonly float Money;

            public readonly bool IsNeighbour;

            public readonly sbyte RawUid;

            public SomeTestHeader(int id, string name, float money, bool isNeighbour, sbyte rawUid)
            {
                Id = id;
                Name = name;
                Money = money;
                IsNeighbour = isNeighbour;
                RawUid = rawUid;
            }

            public bool Equals(SomeTestHeader other)
            {
                return Id == other.Id && Name == other.Name && Money.Equals(other.Money) &&
                       IsNeighbour == other.IsNeighbour && RawUid == other.RawUid;
            }

            public override bool Equals(object obj)
            {
                return obj is SomeTestHeader other && Equals(other);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = Id;
                    hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ Money.GetHashCode();
                    hashCode = (hashCode * 397) ^ IsNeighbour.GetHashCode();
                    hashCode = (hashCode * 397) ^ RawUid.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return
                    $"{nameof(Id)}: {Id}\n{nameof(Name)}: {Name}\n{nameof(Money)}: {Money}\n{nameof(IsNeighbour)}: {IsNeighbour}\n{nameof(RawUid)}: {RawUid}";
            }
        }

        // A Test behaves as an ordinary method
        [Test]
        public void CrazyJsonRawSerializeTest()
        {
            var header = new SomeTestHeader(
                42,
                "Berazold",
                1.2904f,
                false,
                -18
            );
            var writer = new JsonWriter();
            writer.WriteBeginObject();

            writer.WritePropertyName("Id");
            writer.WriteInt(header.Id);
            writer.WriteValueSeparator();

            writer.WritePropertyName("Name");
            writer.WriteString(header.Name);
            writer.WriteValueSeparator();

            writer.WritePropertyName("Money");
            writer.WriteSingle(header.Money);
            writer.WriteValueSeparator();

            writer.WritePropertyName("IsNeighbour");
            writer.WriteBoolean(header.IsNeighbour);
            writer.WriteValueSeparator();

            writer.WritePropertyName("RawUid");
            writer.WriteSbyte(header.RawUid);

            writer.WriteEndObject();

            var buffer = writer.ToUtf8Array();
            Debug.Log(writer.ToString());

            var reader = new JsonReader(buffer);
            reader.ReadIsBeginObject();

            reader.ReadIsPropertyNameWithSeparator();
            var id = reader.ReadInt();
            reader.ReadIsValueSeparator();

            reader.ReadIsPropertyNameWithSeparator();
            var name = reader.ReadString();
            reader.ReadIsValueSeparator();

            reader.ReadIsPropertyNameWithSeparator();
            var money = reader.ReadSingle();
            reader.ReadIsValueSeparator();

            reader.ReadIsPropertyNameWithSeparator();
            var neighbour = reader.ReadBoolean();
            reader.ReadIsValueSeparator();

            reader.ReadIsPropertyNameWithSeparator();
            var uid = reader.ReadSbyte();

            reader.ReadIsEndObject();

            var readerHeader = new SomeTestHeader(
                id,
                name,
                money,
                neighbour,
                uid
            );
            Debug.Log(readerHeader.ToString());
            Assert.AreEqual(header, readerHeader);
        }
    }
}
