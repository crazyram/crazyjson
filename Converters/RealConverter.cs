﻿using System.Runtime.CompilerServices;
using CrazyRam.Core.Serialization.Json.Internal;

namespace CrazyRam.Core.Serialization.Json
{
    public static class RealConverter
    {
        #region Read

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float ReadSingle(byte[] buffer, int offset, out int readCount)
        {
            return unchecked((float) RealConverterInternal.StringToIeee(
                new RealConverterInternal.Iterator(buffer, offset), buffer.Length - offset, false, out readCount));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double ReadDouble(byte[] buffer, int offset, out int readCount)
        {
            return RealConverterInternal.StringToIeee(new RealConverterInternal.Iterator(buffer, offset),
                buffer.Length - offset, true, out readCount);
        }

        #endregion

        #region Write

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteSingle(ref byte[] buffer, int offset, float value)
        {
            return RealToStringConverter.GetBytes(ref buffer, offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteDouble(ref byte[] buffer, int offset, double value)
        {
            return RealToStringConverter.GetBytes(ref buffer, offset, value);
        }

        #endregion
    }
}
