﻿using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Serialization.Json
{
    public static class IntegerConverter
    {
        #region Helper

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsDigit(byte c)
        {
            return c >= (byte) '0' && c <= (byte) '9';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsNumber(byte c)
        {
            switch (c)
            {
                case 43: // +
                //, - 44 excluded
                case 45: // -
                case 46: // .
                // / - 47 excluded
                case 48: // 0
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57: // 9
                    return true;
                default:
                    return false;
            }
        }

        #endregion

        #region ReadSigned

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static sbyte ReadInt8(byte[] buffer, int offset, out int readCount)
        {
            return checked((sbyte) ReadInt64(buffer, offset, out readCount));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static short ReadInt16(byte[] buffer, int offset, out int readCount)
        {
            return checked((short) ReadInt64(buffer, offset, out readCount));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int ReadInt32(byte[] buffer, int offset, out int readCount)
        {
            return checked((int) ReadInt64(buffer, offset, out readCount));
        }

        public static long ReadInt64(byte[] buffer, int offset, out int readcount)
        {
            var result = 0L;
            var sign = buffer[offset] == '-' ? -1 : 1;

            for (int i = ((sign == -1) ? offset + 1 : offset); i < buffer.Length; i++)
            {
                if (!IsDigit(buffer[i]))
                {
                    readcount = i - offset;
                    goto END;
                }

                // result = unchecked(result * 10 + (buffer[i] - '0'));

                result = unchecked((result << 3) + (result << 1) + (buffer[i] - '0'));
            }

            readcount = buffer.Length - offset;

            END:
            return unchecked(result * sign);
        }

        #endregion

        #region ReadUnsigned

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte ReadUint8(byte[] buffer, int offset, out int readCount)
        {
            return checked((byte) ReadUint64(buffer, offset, out readCount));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ushort ReadUint16(byte[] buffer, int offset, out int readCount)
        {
            return checked((ushort) ReadUint64(buffer, offset, out readCount));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint ReadUint32(byte[] buffer, int offset, out int readCount)
        {
            return checked((uint) ReadUint64(buffer, offset, out readCount));
        }

        public static ulong ReadUint64(byte[] buffer, int offset, out int readcount)
        {
            var result = 0UL;

            for (int i = offset; i < buffer.Length; i++)
            {
                if (!IsDigit(buffer[i]))
                {
                    readcount = i - offset;
                    goto END;
                }

                // result = checked(result * 10UL + (ulong) (buffer[i] - '0'));
                result = checked(result * 10UL + (ulong) (buffer[i] - '0'));
            }

            readcount = buffer.Length - offset;

            END:
            return result;
        }

        #endregion

        #region WriteSigned

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteInt8(ref byte[] buffer, int offset, sbyte value)
        {
            return WriteInt64(ref buffer, offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteInt16(ref byte[] buffer, int offset, short value)
        {
            return WriteInt64(ref buffer, offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteInt32(ref byte[] buffer, int offset, int value)
        {
            return WriteInt64(ref buffer, offset, value);
        }

        public static int WriteInt64(ref byte[] buffer, int offset, long value)
        {
            var startOffset = offset;

            long num1 = value, num2, num3, num4, num5, div;

            if (value < 0)
            {
                //long MinValue = -9223372036854775808;
                if (value == long.MinValue)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 20);
                    buffer[offset++] = (byte) '-';
                    buffer[offset++] = (byte) '9';
                    buffer[offset++] = (byte) '2';
                    buffer[offset++] = (byte) '2';
                    buffer[offset++] = (byte) '3';
                    buffer[offset++] = (byte) '3';
                    buffer[offset++] = (byte) '7';
                    buffer[offset++] = (byte) '2';
                    buffer[offset++] = (byte) '0';
                    buffer[offset++] = (byte) '3';
                    buffer[offset++] = (byte) '6';
                    buffer[offset++] = (byte) '8';
                    buffer[offset++] = (byte) '5';
                    buffer[offset++] = (byte) '4';
                    buffer[offset++] = (byte) '7';
                    buffer[offset++] = (byte) '7';
                    buffer[offset++] = (byte) '5';
                    buffer[offset++] = (byte) '8';
                    buffer[offset++] = (byte) '0';
                    buffer[offset++] = (byte) '8';
                    return offset - startOffset;
                }

                JsonUtils.EnsureCapacity(ref buffer, offset, 1);
                buffer[offset++] = (byte) '-';
                num1 = unchecked(-value);
            }

            if (num1 < 10000)
            {
                if (num1 < 10)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 1);
                    goto L1;
                }

                if (num1 < 100)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 2);
                    goto L2;
                }

                if (num1 < 1000)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 3);
                    goto L3;
                }

                JsonUtils.EnsureCapacity(ref buffer, offset, 4);
                goto L4;
            }
            else
            {
                num2 = num1 / 10000;
                num1 -= num2 * 10000;
                if (num2 < 10000)
                {
                    if (num2 < 10)
                    {
                        JsonUtils.EnsureCapacity(ref buffer, offset, 5);
                        goto L5;
                    }

                    if (num2 < 100)
                    {
                        JsonUtils.EnsureCapacity(ref buffer, offset, 6);
                        goto L6;
                    }

                    if (num2 < 1000)
                    {
                        JsonUtils.EnsureCapacity(ref buffer, offset, 7);
                        goto L7;
                    }

                    JsonUtils.EnsureCapacity(ref buffer, offset, 8);
                    goto L8;
                }
                else
                {
                    num3 = num2 / 10000;
                    num2 -= num3 * 10000;
                    if (num3 < 10000)
                    {
                        if (num3 < 10)
                        {
                            JsonUtils.EnsureCapacity(ref buffer, offset, 9);
                            goto L9;
                        }

                        if (num3 < 100)
                        {
                            JsonUtils.EnsureCapacity(ref buffer, offset, 10);
                            goto L10;
                        }

                        if (num3 < 1000)
                        {
                            JsonUtils.EnsureCapacity(ref buffer, offset, 11);
                            goto L11;
                        }

                        JsonUtils.EnsureCapacity(ref buffer, offset, 12);
                        goto L12;
                    }
                    else
                    {
                        num4 = num3 / 10000;
                        num3 -= num4 * 10000;
                        if (num4 < 10000)
                        {
                            if (num4 < 10)
                            {
                                JsonUtils.EnsureCapacity(ref buffer, offset, 13);
                                goto L13;
                            }

                            if (num4 < 100)
                            {
                                JsonUtils.EnsureCapacity(ref buffer, offset, 14);
                                goto L14;
                            }

                            if (num4 < 1000)
                            {
                                JsonUtils.EnsureCapacity(ref buffer, offset, 15);
                                goto L15;
                            }

                            JsonUtils.EnsureCapacity(ref buffer, offset, 16);
                            goto L16;
                        }
                        else
                        {
                            num5 = num4 / 10000;
                            num4 -= num5 * 10000;
                            if (num5 < 10000)
                            {
                                if (num5 < 10)
                                {
                                    JsonUtils.EnsureCapacity(ref buffer, offset, 17);
                                    goto L17;
                                }

                                if (num5 < 100)
                                {
                                    JsonUtils.EnsureCapacity(ref buffer, offset, 18);
                                    goto L18;
                                }

                                if (num5 < 1000)
                                {
                                    JsonUtils.EnsureCapacity(ref buffer, offset, 19);
                                    goto L19;
                                }

                                JsonUtils.EnsureCapacity(ref buffer, offset, 20);
                                goto L20;
                            }

                            L20:
                            buffer[offset++] = (byte) ('0' + (div = (num5 * 8389L) >> 23));
                            num5 -= div * 1000;
                            L19:
                            buffer[offset++] = (byte) ('0' + (div = (num5 * 5243L) >> 19));
                            num5 -= div * 100;
                            L18:
                            buffer[offset++] = (byte) ('0' + (div = (num5 * 6554L) >> 16));
                            num5 -= div * 10;
                            L17:
                            buffer[offset++] = (byte) ('0' + (num5));
                        }

                        L16:
                        buffer[offset++] = (byte) ('0' + (div = (num4 * 8389L) >> 23));
                        num4 -= div * 1000;
                        L15:
                        buffer[offset++] = (byte) ('0' + (div = (num4 * 5243L) >> 19));
                        num4 -= div * 100;
                        L14:
                        buffer[offset++] = (byte) ('0' + (div = (num4 * 6554L) >> 16));
                        num4 -= div * 10;
                        L13:
                        buffer[offset++] = (byte) ('0' + (num4));
                    }

                    L12:
                    buffer[offset++] = (byte) ('0' + (div = (num3 * 8389L) >> 23));
                    num3 -= div * 1000;
                    L11:
                    buffer[offset++] = (byte) ('0' + (div = (num3 * 5243L) >> 19));
                    num3 -= div * 100;
                    L10:
                    buffer[offset++] = (byte) ('0' + (div = (num3 * 6554L) >> 16));
                    num3 -= div * 10;
                    L9:
                    buffer[offset++] = (byte) ('0' + (num3));
                }

                L8:
                buffer[offset++] = (byte) ('0' + (div = (num2 * 8389L) >> 23));
                num2 -= div * 1000;
                L7:
                buffer[offset++] = (byte) ('0' + (div = (num2 * 5243L) >> 19));
                num2 -= div * 100;
                L6:
                buffer[offset++] = (byte) ('0' + (div = (num2 * 6554L) >> 16));
                num2 -= div * 10;
                L5:
                buffer[offset++] = (byte) ('0' + (num2));
            }

            L4:
            buffer[offset++] = (byte) ('0' + (div = (num1 * 8389L) >> 23));
            num1 -= div * 1000;
            L3:
            buffer[offset++] = (byte) ('0' + (div = (num1 * 5243L) >> 19));
            num1 -= div * 100;
            L2:
            buffer[offset++] = (byte) ('0' + (div = (num1 * 6554L) >> 16));
            num1 -= div * 10;
            L1:
            buffer[offset++] = (byte) ('0' + (num1));

            return offset - startOffset;
        }

        #endregion

        #region WriteUnsigned

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteUint8(ref byte[] buffer, int offset, byte value)
        {
            return WriteUint64(ref buffer, offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteUint16(ref byte[] buffer, int offset, ushort value)
        {
            return WriteUint64(ref buffer, offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int WriteUint32(ref byte[] buffer, int offset, uint value)
        {
            return WriteUint64(ref buffer, offset, value);
        }

        public static int WriteUint64(ref byte[] buffer, int offset, ulong value)
        {
            var startOffset = offset;

            ulong num1 = value, num2, num3, num4, num5, div;

            if (num1 < 10000)
            {
                if (num1 < 10)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 1);
                    goto L1;
                }

                if (num1 < 100)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 2);
                    goto L2;
                }

                if (num1 < 1000)
                {
                    JsonUtils.EnsureCapacity(ref buffer, offset, 3);
                    goto L3;
                }

                JsonUtils.EnsureCapacity(ref buffer, offset, 4);
                goto L4;
            }
            else
            {
                num2 = num1 / 10000;
                num1 -= num2 * 10000;
                if (num2 < 10000)
                {
                    if (num2 < 10)
                    {
                        JsonUtils.EnsureCapacity(ref buffer, offset, 5);
                        goto L5;
                    }

                    if (num2 < 100)
                    {
                        JsonUtils.EnsureCapacity(ref buffer, offset, 6);
                        goto L6;
                    }

                    if (num2 < 1000)
                    {
                        JsonUtils.EnsureCapacity(ref buffer, offset, 7);
                        goto L7;
                    }

                    JsonUtils.EnsureCapacity(ref buffer, offset, 8);
                    goto L8;
                }
                else
                {
                    num3 = num2 / 10000;
                    num2 -= num3 * 10000;
                    if (num3 < 10000)
                    {
                        if (num3 < 10)
                        {
                            JsonUtils.EnsureCapacity(ref buffer, offset, 9);
                            goto L9;
                        }

                        if (num3 < 100)
                        {
                            JsonUtils.EnsureCapacity(ref buffer, offset, 10);
                            goto L10;
                        }

                        if (num3 < 1000)
                        {
                            JsonUtils.EnsureCapacity(ref buffer, offset, 11);
                            goto L11;
                        }

                        JsonUtils.EnsureCapacity(ref buffer, offset, 12);
                        goto L12;
                    }
                    else
                    {
                        num4 = num3 / 10000;
                        num3 -= num4 * 10000;
                        if (num4 < 10000)
                        {
                            if (num4 < 10)
                            {
                                JsonUtils.EnsureCapacity(ref buffer, offset, 13);
                                goto L13;
                            }

                            if (num4 < 100)
                            {
                                JsonUtils.EnsureCapacity(ref buffer, offset, 14);
                                goto L14;
                            }

                            if (num4 < 1000)
                            {
                                JsonUtils.EnsureCapacity(ref buffer, offset, 15);
                                goto L15;
                            }

                            JsonUtils.EnsureCapacity(ref buffer, offset, 16);
                            goto L16;
                        }
                        else
                        {
                            num5 = num4 / 10000;
                            num4 -= num5 * 10000;
                            if (num5 < 10000)
                            {
                                if (num5 < 10)
                                {
                                    JsonUtils.EnsureCapacity(ref buffer, offset, 17);
                                    goto L17;
                                }

                                if (num5 < 100)
                                {
                                    JsonUtils.EnsureCapacity(ref buffer, offset, 18);
                                    goto L18;
                                }

                                if (num5 < 1000)
                                {
                                    JsonUtils.EnsureCapacity(ref buffer, offset, 19);
                                    goto L19;
                                }

                                JsonUtils.EnsureCapacity(ref buffer, offset, 20);
                                goto L20;
                            }

                            L20:
                            buffer[offset++] = (byte) ('0' + (div = (num5 * 8389UL) >> 23));
                            num5 -= div * 1000;
                            L19:
                            buffer[offset++] = (byte) ('0' + (div = (num5 * 5243UL) >> 19));
                            num5 -= div * 100;
                            L18:
                            buffer[offset++] = (byte) ('0' + (div = (num5 * 6554UL) >> 16));
                            num5 -= div * 10;
                            L17:
                            buffer[offset++] = (byte) ('0' + (num5));
                        }

                        L16:
                        buffer[offset++] = (byte) ('0' + (div = (num4 * 8389UL) >> 23));
                        num4 -= div * 1000;
                        L15:
                        buffer[offset++] = (byte) ('0' + (div = (num4 * 5243UL) >> 19));
                        num4 -= div * 100;
                        L14:
                        buffer[offset++] = (byte) ('0' + (div = (num4 * 6554UL) >> 16));
                        num4 -= div * 10;
                        L13:
                        buffer[offset++] = (byte) ('0' + (num4));
                    }

                    L12:
                    buffer[offset++] = (byte) ('0' + (div = (num3 * 8389UL) >> 23));
                    num3 -= div * 1000;
                    L11:
                    buffer[offset++] = (byte) ('0' + (div = (num3 * 5243UL) >> 19));
                    num3 -= div * 100;
                    L10:
                    buffer[offset++] = (byte) ('0' + (div = (num3 * 6554UL) >> 16));
                    num3 -= div * 10;
                    L9:
                    buffer[offset++] = (byte) ('0' + (num3));
                }

                L8:
                buffer[offset++] = (byte) ('0' + (div = (num2 * 8389UL) >> 23));
                num2 -= div * 1000;
                L7:
                buffer[offset++] = (byte) ('0' + (div = (num2 * 5243UL) >> 19));
                num2 -= div * 100;
                L6:
                buffer[offset++] = (byte) ('0' + (div = (num2 * 6554UL) >> 16));
                num2 -= div * 10;
                L5:
                buffer[offset++] = (byte) ('0' + (num2));
            }

            L4:
            buffer[offset++] = (byte) ('0' + (div = (num1 * 8389UL) >> 23));
            num1 -= div * 1000;
            L3:
            buffer[offset++] = (byte) ('0' + (div = (num1 * 5243UL) >> 19));
            num1 -= div * 100;
            L2:
            buffer[offset++] = (byte) ('0' + (div = (num1 * 6554UL) >> 16));
            num1 -= div * 10;
            L1:
            buffer[offset++] = (byte) ('0' + (num1));

            return offset - startOffset;
        }

        #endregion
    }
}
