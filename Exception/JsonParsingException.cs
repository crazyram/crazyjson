﻿using System;

namespace CrazyRam.Core.Serialization.Json
{
    public class JsonParsingException : Exception
    {
        public JsonParsingException(string message, byte[] buffer = null, int position = -1) : base(
            $"{message}.{PrepareErrorChunk(buffer, position)}")
        {
        }

        private static unsafe string PrepareErrorChunk(byte[] buffer, int position)
        {
            if (buffer == null || position < 0)
                return string.Empty;
            int min = System.Math.Max(0, position - 20);
            int max = System.Math.Min(buffer.Length, position + 20);
            int length = max - min;

            string result;
            fixed (byte* ptr = buffer)
            {
                result = StringEncoding.Utf8.GetString(ptr + min, length);
            }

            return $"{result} {(char) buffer[position - 1]}{(char) buffer[position]}";
        }
    }
}
