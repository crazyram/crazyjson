﻿using System;

public class JsonBuilderNotRegisteredException : Exception
{
    public JsonBuilderNotRegisteredException(string message) : base(message)
    {
    }
}
