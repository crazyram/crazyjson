﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace CrazyRam.Core.Serialization.Json
{
    public static class StringEncoding
    {
        public static readonly Encoding Utf8 = new UTF8Encoding(false);
    }

    public struct JsonWriter
    {
        private byte[] _buffer;

        private int _offset;

        public byte[] Buffer
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _buffer;
        }

        public int Offset
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _offset;
        }

        public JsonWriter(byte[] buffer, int offset = 0)
        {
            _buffer = buffer;
            _offset = offset;
        }

        public void AdvanceOffset(int count)
        {
            _offset += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void EnsureCapacity(int append)
        {
            JsonUtils.EnsureCapacity(ref _buffer, _offset, append);
        }

        #region Export

        public byte[] ToUtf8Array()
        {
            return _buffer == null
                ? Array.Empty<byte>()
                : JsonUtils.FastCloneWithResize(_buffer, _offset);
        }

        public override string ToString()
        {
            return _buffer == null
                ? "Invalid JsonReader"
                : StringEncoding.Utf8.GetString(_buffer, 0, _offset);
        }

        public static byte[] GetEncodedPropertyName(string propertyName)
        {
            var writer = new JsonWriter();
            writer.WritePropertyName(propertyName);
            return writer.ToUtf8Array();
        }

        public static byte[] GetEncodedPropertyNameWithPrefixValueSeparator(string propertyName)
        {
            var writer = new JsonWriter();
            writer.WriteValueSeparator();
            writer.WritePropertyName(propertyName);
            return writer.ToUtf8Array();
        }

        public static byte[] GetEncodedPropertyNameWithBeginObject(string propertyName)
        {
            var writer = new JsonWriter();
            writer.WriteBeginObject();
            writer.WritePropertyName(propertyName);
            return writer.ToUtf8Array();
        }

        public static byte[] GetEncodedPropertyNameWithoutQuotation(string propertyName)
        {
            var writer = new JsonWriter();
            writer.WriteString(propertyName);
            var buf = writer._buffer;
            var result = new byte[buf.Length - 2];
            System.Buffer.BlockCopy(buf, 1, result, 0, result.Length);
            return result;
        }

        #endregion

        #region SpeciaWriter

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBeginArray()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) '[';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteEndArray()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) ']';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBeginObject()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) '{';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteEndObject()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) '}';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteValueSeparator()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) ',';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteNameSeparator()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) ':';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteQuotation()
        {
            EnsureCapacity(1);
            _buffer[_offset++] = (byte) '\"';
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WritePropertyName(string name)
        {
            WriteString(name);
            WriteNameSeparator();
        }

        public void WritePropertyNameRaw(byte[] name)
        {
            WriteRaw(name);
            WriteNameSeparator();
        }

        #endregion

        #region ValueWriter

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteRaw(byte[] bytes)
        {
            EnsureCapacity(bytes.Length);
            //todo check faster impl
            System.Buffer.BlockCopy(bytes, 0, _buffer, _offset, bytes.Length);
            _offset += bytes.Length;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteRaw(byte value)
        {
            EnsureCapacity(1);
            _buffer[_offset++] = value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteSingle(float value)
        {
            _offset += RealConverter.WriteSingle(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteDouble(double value)
        {
            _offset += RealConverter.WriteDouble(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteSbyte(sbyte value)
        {
            _offset += IntegerConverter.WriteInt8(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteByte(byte value)
        {
            _offset += IntegerConverter.WriteUint8(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteShort(short value)
        {
            _offset += IntegerConverter.WriteInt16(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteUshort(ushort value)
        {
            _offset += IntegerConverter.WriteUint16(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteInt(int value)
        {
            _offset += IntegerConverter.WriteInt32(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteUint(uint value)
        {
            _offset += IntegerConverter.WriteUint32(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteLong(long value)
        {
            _offset += IntegerConverter.WriteInt64(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteUlong(ulong value)
        {
            _offset += IntegerConverter.WriteUint64(ref _buffer, _offset, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteNull()
        {
            JsonUtils.EnsureCapacity(ref _buffer, _offset, 4);
            _buffer[_offset] = (byte) 'n';
            _buffer[_offset + 1] = (byte) 'u';
            _buffer[_offset + 2] = (byte) 'l';
            _buffer[_offset + 3] = (byte) 'l';

            _offset += 4;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBoolean(bool value)
        {
            if (value)
                WriteTrue();
            else
                WriteFalse();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteTrue()
        {
            JsonUtils.EnsureCapacity(ref _buffer, _offset, 4);
            _buffer[_offset] = (byte) 't';
            _buffer[_offset + 1] = (byte) 'r';
            _buffer[_offset + 2] = (byte) 'u';
            _buffer[_offset + 3] = (byte) 'e';
            _offset += 4;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteFalse()
        {
            JsonUtils.EnsureCapacity(ref _buffer, _offset, 5);
            _buffer[_offset] = (byte) 'f';
            _buffer[_offset + 1] = (byte) 'a';
            _buffer[_offset + 2] = (byte) 'l';
            _buffer[_offset + 3] = (byte) 's';
            _buffer[_offset + 4] = (byte) 'e';
            _offset += 5;
        }

        public void WriteString(string value)
        {
            if (value == null)
            {
                WriteNull();
                return;
            }

            var startoffset = _offset;
            var max = StringEncoding.Utf8.GetMaxByteCount(value.Length) + 2;
            JsonUtils.EnsureCapacity(ref _buffer, startoffset, max);

            var from = 0;

            _buffer[_offset++] = (byte) '\"';

            // for JIT Optimization, for-loop i < str.Length
            for (int i = 0; i < value.Length; i++)
            {
                // ReSharper disable once RedundantAssignment
                byte escapeChar = default;
                switch (value[i])
                {
                    case '"':
                        escapeChar = (byte) '"';
                        break;
                    case '\\':
                        escapeChar = (byte) '\\';
                        break;
                    case '\b':
                        escapeChar = (byte) 'b';
                        break;
                    case '\f':
                        escapeChar = (byte) 'f';
                        break;
                    case '\n':
                        escapeChar = (byte) 'n';
                        break;
                    case '\r':
                        escapeChar = (byte) 'r';
                        break;
                    case '\t':
                        escapeChar = (byte) 't';
                        break;
                    default:
                        continue;
                }

                max += 2;
                JsonUtils.EnsureCapacity(ref _buffer, startoffset, max); // check +escape capacity

                _offset += StringEncoding.Utf8.GetBytes(value, from, i - from, _buffer, _offset);
                from = i + 1;
                _buffer[_offset++] = (byte) '\\';
                _buffer[_offset++] = escapeChar;
            }

            if (from != value.Length)
            {
                _offset += StringEncoding.Utf8.GetBytes(value, from, value.Length - from, _buffer, _offset);
            }

            _buffer[_offset++] = (byte) '\"';
        }

        #endregion
    }
}
