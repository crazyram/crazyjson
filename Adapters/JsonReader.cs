﻿using System;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Serialization.Json
{
    public struct JsonReader
    {
        //todo use raw pointer
        private readonly byte[] _buffer;

        private int _offset;

        private bool InRange
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _offset < _buffer.Length;
        }

        public byte[] Buffer
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _buffer;
        }

        public int Offset
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => _offset;
        }

        private static readonly byte[] Preamble = StringEncoding.Utf8.GetPreamble();

        public JsonReader(byte[] buffer, int offset = 0)
        {
            _buffer = buffer;
            _offset = offset;

            if (buffer.Length >= 3 && Preamble != null && Preamble.Length >= 3 && Preamble.Length >= 3)
            {
                if (_buffer[offset] == Preamble[0] && _buffer[offset + 1] == Preamble[1] &&
                    _buffer[offset + 2] == Preamble[2])
                    _offset += 3;
            }
        }

        public void AdvanceOffset(int offset)
        {
            _offset += offset;
        }

#region SpecialReader

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SkipWhitespace()
        {
            for (int i = _offset; i < _buffer.Length; i++)
            {
                switch (_buffer[i])
                {
                    case 0x20:
                    case 0x09:
                    case 0x0A:
                    case 0x0D:
                        continue;
                    case (byte) '/':
                        i = ReadComment(_buffer, i);
                        continue;
                    default:
                        _offset = i;
                        return;
                }
            }

            _offset = _buffer.Length;
        }

        public bool ReadIsNull()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == 'n')
            {
                if (_buffer[_offset + 1] != 'u' ||
                    _buffer[_offset + 2] != 'l' ||
                    _buffer[_offset + 3] != 'l')
                    throw new JsonParsingException("Read 'null' exception");
                _offset += 4;
                return true;
            }

            return false;
        }

        public bool ReadIsBeginArray()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == '[')
            {
                _offset++;
                return true;
            }

            return false;
        }

        public bool ReadIsEndArray()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == ']')
            {
                _offset++;
                return true;
            }

            return false;
        }

        public bool ReadIsEndArrayWithValueSeparator(ref int count)
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == ']')
            {
                _offset++;
                return true;
            }

            if (count++ != 0)
                ReadIsValueSeparator();
            return false;
        }

        public bool ReadIsInArray(ref int count)
        {
            if (count == 0)
            {
                ReadIsBeginArray();
                if (ReadIsEndArray())
                    return false;
            }
            else
            {
                if (ReadIsEndArray())
                    return false;
                ReadIsValueSeparator();
            }

            count++;
            return true;
        }

        public bool ReadIsBeginObject()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == '{')
            {
                _offset += 1;
                return true;
            }

            return false;
        }

        public bool ReadIsEndObject()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == '}')
            {
                _offset += 1;
                return true;
            }

            return false;
        }

        public bool ReadIsEndObjectWithValueSeparator(ref int count)
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == '}')
            {
                _offset++;
                return true;
            }

            if (count++ != 0)
                ReadIsValueSeparator();
            return false;
        }

        public bool ReadIsInObject(ref int count)
        {
            if (count == 0)
            {
                ReadIsBeginObject();
                if (ReadIsEndObject())
                    return false;
            }

            if (ReadIsEndObject())
                return false;
            ReadIsValueSeparator();

            count++;
            return true;
        }

        public bool ReadIsValueSeparator()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == ',')
            {
                _offset++;
                return true;
            }

            return false;
        }

        public bool ReadIsNameSeparator()
        {
            SkipWhitespace();
            if (InRange && _buffer[_offset] == ':')
            {
                _offset++;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Dangerous! string allocation here
        /// </summary>
        public string ReadPropertyName()
        {
            var key = ReadString();
            ReadIsNameSeparator();
            return key;
        }

        public bool IsPropertyNameMatch(string name)
        {
            SkipWhitespace();
            for (int i = _offset, j = 0; i < _buffer.Length && j < name.Length; i++, j++)
            {
                if (_buffer[i] != name[j])
                    return false;
            }

            return true;
        }

        public bool IsNameCharMatch(char c)
        {
            return _buffer[_offset] == c;
        }

        public bool ReadIsPropertyNameWithSeparator()
        {
            SkipWhitespace();
            for (int i = _offset; i < _buffer.Length; i++)
            {
                switch (_buffer[i])
                {
                    case (byte) ':':
                        _offset = i + 1;
                        return true;
                    case (byte) ',':
                    case (byte) '\n':
                    case (byte) '\r':
                        throw new JsonParsingException("Cannot read property name");
                    default:
                        continue;
                }
            }

            return false;
        }

#endregion

        private void ReadStringSegmentCore(out byte[] result, out int resultOffset, out int resultLength)
        {
            var builder = StringBuilderCache.Buffer;
            var builderOffset = 0;

            var pointStringBuilder = StringBuilderCache.PointStringBuffer;
            var pointStringOffset = 0;

            if (_buffer[_offset] != '\"')
                throw new JsonParsingException("Cannot read string");

            _offset++;
            var from = _offset;

            for (int i = _offset; i < _buffer.Length; i++)
            {
                byte escapeChar = 0;
                switch (_buffer[i])
                {
                    case (byte) '\\': // escape character
                        switch ((char) _buffer[i + 1])
                        {
                            case '"':
                            case '\\':
                            case '/':
                                escapeChar = _buffer[i + 1];
                                goto COPY;
                            case 'b':
                                escapeChar = (byte) '\b';
                                goto COPY;
                            case 'f':
                                escapeChar = (byte) '\f';
                                goto COPY;
                            case 'n':
                                escapeChar = (byte) '\n';
                                goto COPY;
                            case 'r':
                                escapeChar = (byte) '\r';
                                goto COPY;
                            case 't':
                                escapeChar = (byte) '\t';
                                goto COPY;
                            case 'u':

                                if (pointStringOffset == 0)
                                {
                                    var copyCount = i - from;
                                    JsonUtils.EnsureCapacity(ref builder, builderOffset, copyCount + 1); // require + 1
                                    System.Buffer.BlockCopy(_buffer, from, builder, builderOffset, copyCount);
                                    builderOffset += copyCount;
                                }

                                if (pointStringBuilder.Length == pointStringOffset)
                                    Array.Resize(ref pointStringBuilder, pointStringBuilder.Length * 2);

                                var a = (char) _buffer[i + 2];
                                var b = (char) _buffer[i + 3];
                                var c = (char) _buffer[i + 4];
                                var d = (char) _buffer[i + 5];
                                var codepoint = GetCodePoint(a, b, c, d);
                                pointStringBuilder[pointStringOffset++] = (char) codepoint;
                                i += 5;
                                _offset += 6;
                                from = _offset;
                                continue;
                            default:
                                throw new JsonParsingException("Bad JSON escape.");
                        }
                    case (byte) '"': // endtoken
                        _offset++;
                        goto END;
                    default: // string
                        if (pointStringOffset != 0)
                        {
                            JsonUtils.EnsureCapacity(ref builder, builderOffset,
                                StringEncoding.Utf8.GetMaxByteCount(pointStringOffset));
                            builderOffset += StringEncoding.Utf8.GetBytes(pointStringBuilder, 0, pointStringOffset,
                                builder, builderOffset);
                            pointStringOffset = 0;
                        }

                        _offset++;
                        continue;
                }

                COPY:
                {
                    if (pointStringOffset != 0)
                    {
                        JsonUtils.EnsureCapacity(ref builder, builderOffset,
                            StringEncoding.Utf8.GetMaxByteCount(pointStringOffset));
                        builderOffset += StringEncoding.Utf8.GetBytes(pointStringBuilder, 0, pointStringOffset,
                            builder, builderOffset);
                        pointStringOffset = 0;
                    }

                    var copyCount = i - from;
                    JsonUtils.EnsureCapacity(ref builder, builderOffset, copyCount + 1); // require + 1!
                    System.Buffer.BlockCopy(_buffer, from, builder, builderOffset, copyCount);
                    builderOffset += copyCount;
                    builder[builderOffset++] = escapeChar;
                    i += 1;
                    _offset += 2;
                    from = _offset;
                }
            }

            resultLength = 0;
            result = null;
            resultOffset = 0;
            throw new JsonParsingException("String End Token");

            END:
            if (builderOffset == 0 && pointStringOffset == 0) // no escape
            {
                result = _buffer;
                resultOffset = from;
                resultLength = _offset - 1 - from; // skip last quote
            }
            else
            {
                if (pointStringOffset != 0)
                {
                    JsonUtils.EnsureCapacity(ref builder, builderOffset,
                        StringEncoding.Utf8.GetMaxByteCount(pointStringOffset));
                    builderOffset += StringEncoding.Utf8.GetBytes(pointStringBuilder, 0, pointStringOffset,
                        builder, builderOffset);
                    pointStringOffset = 0;
                }

                var copyCount = _offset - from - 1;
                JsonUtils.EnsureCapacity(ref builder, builderOffset, copyCount);
                System.Buffer.BlockCopy(_buffer, from, builder, builderOffset, copyCount);
                builderOffset += copyCount;

                result = builder;
                resultOffset = 0;
                resultLength = builderOffset;
            }
        }

#region ValueReader

        public bool ReadBoolean()
        {
            SkipWhitespace();

            if (_buffer[_offset] == 't')
            {
                if (_buffer[_offset + 1] != 'r' || _buffer[_offset + 2] != 'u' || _buffer[_offset + 3] != 'e')
                    throw new JsonParsingException("Cannot read true");

                _offset += 4;
                return true;
            }

            if (_buffer[_offset] == 'f')
            {
                if (_buffer[_offset + 1] != 'a' || _buffer[_offset + 2] != 'l' || _buffer[_offset + 3] != 's' ||
                    _buffer[_offset + 4] != 'e')
                    throw new JsonParsingException("Cannot read false");

                _offset += 5;
                return false;
            }

            throw new JsonParsingException("Cannot read boolean");
        }

        public string ReadString()
        {
            if (ReadIsNull())
                return null;

            ReadStringSegmentCore(out var bytes, out var offset, out var length);
            return StringEncoding.Utf8.GetString(bytes, offset, length);
        }

        public byte ReadByte()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadUint8(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read byte");
            _offset += readCount;
            return value;
        }

        public sbyte ReadSbyte()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadInt8(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read sbyte");
            _offset += readCount;
            return value;
        }

        public ushort ReadUshort()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadUint16(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read ushort");
            _offset += readCount;
            return value;
        }

        public short ReadShort()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadInt16(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read short");
            _offset += readCount;
            return value;
        }

        public uint ReadUint()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadUint32(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read uint");
            _offset += readCount;
            return value;
        }

        public int ReadInt()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadInt32(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read int");
            _offset += readCount;
            return value;
        }

        public ulong ReadUlong()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadUint64(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read ulong");
            _offset += readCount;
            return value;
        }

        public long ReadLong()
        {
            SkipWhitespace();
            var value = IntegerConverter.ReadInt64(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read long");
            _offset += readCount;
            return value;
        }

        public float ReadSingle()
        {
            SkipWhitespace();
            var value = RealConverter.ReadSingle(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException($"Cannot read float at: {_offset}", _buffer, _offset);
            _offset += readCount;
            return value;
        }

        public double ReadDouble()
        {
            SkipWhitespace();
            var value = RealConverter.ReadDouble(_buffer, _offset, out var readCount);
            if (readCount == 0)
                throw new JsonParsingException("Cannot read double");
            _offset += readCount;
            return value;
        }

#endregion

#region Helpers

        private static int ToNumber(char x)
        {
            if ('0' <= x && x <= '9')
                return x - '0';
            if ('a' <= x && x <= 'f')
                return x - 'a' + 10;
            if ('A' <= x && x <= 'F')
                return x - 'A' + 10;
            throw new JsonParsingException($"Invalid character {x}");
        }

        private static int GetCodePoint(char a, char b, char c, char d)
        {
            return (((((ToNumber(a) * 16) + ToNumber(b)) * 16) + ToNumber(c)) * 16) + ToNumber(d);
        }

        public static int ReadComment(byte[] buffer, int start)
        {
            if (buffer[start + 1] == '/')
            {
                start += 2;
                for (int i = start; i < buffer.Length; i++)
                {
                    if (buffer[i] == '\r' || buffer[i] == '\n')
                        return i;
                }

                throw new JsonParsingException("Cannot find end of comment line(\r or \n).");
            }

            if (buffer[start + 1] == '*')
            {
                start += 2;
                for (int i = start; i < buffer.Length; i++)
                {
                    if (buffer[i] == '*' || buffer[i] == '/')
                        return i + 1;
                }

                throw new JsonParsingException("Cannot find end of multi line comment(*/)");
            }

            return start;
        }

        private static class StringBuilderCache
        {
            [ThreadStatic]
            private static byte[] _buffer;

            [ThreadStatic]
            private static char[] _pointStringBuffer;

            public static byte[] Buffer => _buffer ?? (_buffer = new byte[65535]);

            public static char[] PointStringBuffer => _pointStringBuffer ?? (_pointStringBuffer = new char[65535]);
        }

#endregion
    }
}
