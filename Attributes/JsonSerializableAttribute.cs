﻿using System;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
public class JsonSerializableAttribute : Attribute
{
}
