﻿using System;

namespace CrazyRam.Core.Serialization.Json
{
    [AttributeUsage(AttributeTargets.Field)]
    public class JsonArrayCountPropertyAttribute : Attribute
    {
        public readonly string Property;

        public JsonArrayCountPropertyAttribute(string property)
        {
            Property = property;
        }
    }
}
