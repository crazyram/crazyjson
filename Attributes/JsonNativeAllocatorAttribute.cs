﻿using System;
using Unity.Collections;

namespace CrazyRam.Core.Serialization.Json
{
    [AttributeUsage(AttributeTargets.Field)]
    public class JsonNativeAllocatorAttribute : Attribute
    {
        public readonly Allocator Allocator;

        public JsonNativeAllocatorAttribute(Allocator allocator)
        {
            Allocator = allocator;
        }
    }
}
