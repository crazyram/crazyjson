﻿using System;

namespace CrazyRam.Core.Serialization.Json
{
    [AttributeUsage(AttributeTargets.Field)]
    public class JsonOptionalAttribute : Attribute
    {
    }
}
