﻿using System;

namespace CrazyRam.Core.Serialization.Json
{
    /// <summary>
    /// Allocates array with a given capacity
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class JsonArrayCapacityAttribute : Attribute
    {
        public readonly int Capacity;

        public JsonArrayCapacityAttribute(int capacity)
        {
            Capacity = capacity;
        }
    }
}
