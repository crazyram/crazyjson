﻿using System;

namespace CrazyRam.Core.Serialization.Json
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class JsonArrayPoolAttribute : Attribute
    {
        public readonly int AverageSize;

        public JsonArrayPoolAttribute(int averageSize)
        {
            AverageSize = averageSize;
        }
    }
}
