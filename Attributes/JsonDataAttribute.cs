﻿using System;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class JsonDataAttribute : Attribute
{
    public readonly int Id;

    public readonly string NameOverride;

    public JsonDataAttribute(int id = 0, string nameOverride = "")
    {
        Id = id;
        NameOverride = nameOverride;
    }
}
